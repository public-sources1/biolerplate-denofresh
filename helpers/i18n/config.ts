import i18next from 'i18next';
import i18nextMiddleware from 'https://deno.land/x/i18next_http_middleware@v3.5.0/index.js';

import loginupEn from '@helpers/i18n/locales/en/loginup.json' assert { type: 'json' };
import footerEn from '@helpers/i18n/locales/en/footer.json' assert { type: 'json' };
import loginupEs from '@helpers/i18n/locales/es/loginup.json' assert { type: 'json' };
import footerEs from '@helpers/i18n/locales/es/footer.json' assert { type: 'json' };
import homeEs from '@helpers/i18n/locales/es/home.json' assert { type: 'json' };
import homeEn from '@helpers/i18n/locales/en/home.json' assert { type: 'json' };

export const defaultNS = 'home';

export const resources = {
  en: {
    loginup: loginupEn,
    footer: footerEn,
    home: homeEn,
  },
  es: {
    loginup: loginupEs,
    footer: footerEs,
    home: homeEs,
  },
};

export const optionsI18Next = {
  initImmediate: false,
  fallbackLng: 'es',
  preload: ['en', 'es'],
  resources,
  defaultNS,
  saveMissing: true,
};

i18next.use(i18nextMiddleware.LanguageDetector)
  .init({ ...optionsI18Next });

export { i18next };
export const t = i18next.t;
export const handler = i18nextMiddleware.handle(i18next);
