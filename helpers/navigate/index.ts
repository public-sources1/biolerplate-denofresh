export default function navigate(link: string) {
  if (typeof document === 'undefined') {
    throw new Error(
      'The document is undefined. Check if this function is loaded in frontend/islands section',
    );
  }

  const linkTag = document.createElement('div');
  document.querySelector('[f-client-nav]')?.appendChild(linkTag);

  linkTag.innerHTML =
    `<a href="${link}" id="ss-auto-click-sonick" target="" f-partial="${link}">Iniciar sesión</a>`;

  (document.querySelector('#ss-auto-click-sonick') as HTMLButtonElement)?.click();

  document.querySelectorAll('#ss-auto-click-sonick').forEach((element) => {
    element.remove();
  });
}
