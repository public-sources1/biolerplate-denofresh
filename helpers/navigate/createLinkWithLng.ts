import { i18next } from '@helpers/i18n/config.ts';

export default function createLinkWithLng(link: string) {
  const lng = new URLSearchParams(link.split('?')[1]).get('lng');

  return lng
    ? link
    : link.match(/\?/)
    ? `${link}&lng=${i18next.language}`
    : `${link}?lng=${i18next.language}`;
}
