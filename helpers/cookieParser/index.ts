export default class CookieParser {
  #cookieObject: Record<string, string>;
  constructor(cookieString: string) {
    this.#cookieObject = {};
    this.parseCookie(cookieString);
  }

  parseCookie(cookieString: string) {
    const cookiePairs = cookieString.split(';');

    for (const pair of cookiePairs) {
      const [key, value] = pair.trim().split('=');
      this.#cookieObject[key] = decodeURIComponent(value);
    }
  }

  get(key: string) {
    return this.#cookieObject[key];
  }

  set(key: string, value: string) {
    this.#cookieObject[key] = value;
  }

  getAll() {
    return this.#cookieObject;
  }
}
