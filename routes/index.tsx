import { useSignal } from '@preact/signals';
import Counter from '@islands/Counter.tsx';

import LinkSimple from '@components/buttons/linkSimple.tsx'
import GreetSection from '@islands/greetComponent.tsx'

import { t } from '@helpers/i18n/config.ts';

export default function Home() {
  const count = useSignal(3);
  return (
    <div class='px-4 py-8 mx-auto bg-[#86efac]'>
      <div class='max-w-screen-md mx-auto flex flex-col items-center justify-center'>
        <img
          class='my-6'
          src='/logo.svg'
          width='128'
          height='128'
          alt='the Fresh logo: a sliced lemon dripping with juice'
        />
        <h1 class='text-4xl font-bold'>{t('welcome_to_fresh')}</h1>
        <p class='my-4'>
          {t('try_update', {file: './routes/index.tsx', interpolation: {escapeValue: false} })}
        </p>
        <Counter count={count} />

        <LinkSimple link="/greet/carlos" text={t('go_to_meet_page')} />

        <GreetSection/>
      </div>
    </div>
  );
}
