import { FreshContext } from '$fresh/server.ts';
import CookieParser from '@helpers/cookieParser/index.ts';

interface State {
  data: string;
}

export async function handler(
  req: Request,
  ctx: FreshContext<State>,
) {
  ctx.state.data = 'myData';

  const headers = req.headers;
  if (ctx.destination === 'route') {
    const cookieParser = new CookieParser(headers.get('cookie') || '');
    console.log(cookieParser.get('i18next'), 'FT es una cookie');
  }

  const resp = await ctx.next();
  resp.headers.set('server', 'fresh server');
  return resp;
}
