import { PageProps } from '$fresh/server.ts';
import { t } from '@helpers/i18n/config.ts';


export default function Greet(props: PageProps) {
  return <div>{t('hello')} {props.params.name}</div>;
}
