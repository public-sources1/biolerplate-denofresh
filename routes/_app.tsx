import { PageProps } from '$fresh/server.ts';
import { Partial } from '$fresh/runtime.ts';

import { i18next } from '@helpers/i18n/config.ts';
import LoadLocale from '@routes/(_islands)/loadLocale.tsx';

export default function App({ Component, ...props }: PageProps) {
  const lng = new URLSearchParams(props.url.search).get('lng');
  if (lng) {
    i18next.changeLanguage(lng);
  }

  return (
    <html class='dark'>
      <head>
        <meta charset='utf-8' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
        <title>fotogena</title>

        <link rel='stylesheet' href='https://unpkg.com/leaflet@1.7.1/dist/leaflet.css' />
        <link
          rel='stylesheet'
          href='https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css'
        />
        <script src='https://unpkg.com/leaflet@1.7.1/dist/leaflet.js' />
        <script src='https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js' />
        <script src='https://unpkg.com/shapefile@0.6' />
        <script
          type='text/javascript'
          src='https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.6.0/proj4.js'
        />
        <script src='https://cdn.jsdelivr.net/npm/@turf/turf@6/turf.min.js' />
      </head>
      <body f-client-nav>
        <Partial name='body'>
          <LoadLocale lng={lng as 'en' | 'es'} />
          <Component />
        </Partial>
      </body>
    </html>
  );
}
