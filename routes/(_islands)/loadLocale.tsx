import { i18next } from '@helpers/i18n/config.ts';
import ChangeLocale from '@components/utils/translate/changeLocale.tsx';

export default ({ lng }: { lng: 'en' | 'es' }) => {
  i18next.changeLanguage(lng);
  return (
    <div className='absolute right-2 top-2'>
      <ChangeLocale />
    </div>
  );
};
