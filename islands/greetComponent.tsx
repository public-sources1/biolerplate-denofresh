import DefaultInput from '@components/inputs/defaultInput.tsx'
import {eventTypes} from '@components/inputs/types.d.ts'
import Button from '@components/buttons/index.tsx'
import {personName} from '@store/signals/index.ts'
import { t } from '@helpers/i18n/config.ts';
import navidate from '@helpers/navigate/index.ts'
import createLinkWithLng from '@helpers/navigate/createLinkWithLng.ts'

import {useSignal} from '@preact/signals';
import Loading from '@components/utils/loading/loading.tsx'

export default function(){
	const isLoading = useSignal(false)
	function updateName(evt: eventTypes){
		personName.value = (evt?.target as HTMLInputElement).value
	}
	async function goToMeetPage(){
		isLoading.value = true
		await new Promise(resolve=>setTimeout(resolve, 1000))
		navidate(createLinkWithLng(`/greet/${personName.value}`))
	}
	return <div className="pt-10">
		<DefaultInput label={t('name_person')} onInput={updateName} />
		<div className="pt-10"/>
		{isLoading.value?	<Loading text={t('wait_moment')}/>:
	
		<Button onClick={goToMeetPage} text={t(`greet_to`, {person: personName.value})}/>
	}
	</div>
}