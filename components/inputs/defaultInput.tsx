import { inputTypes } from '@components/inputs/types.d.ts';
import { EyeCloseIcon, EyeIcon } from '../icons/index.ts';
import { useState } from 'preact/hooks';

export default function ({ required, password, label, id, onChange, value, onInput }: inputTypes) {
  const idLbale = new Date().getTime();

  const [inputPass, setInputPass] = useState(!!password);

  return (
    <div>
      <div>
        <label for={id || `input-${idLbale}`}>{label}</label>
      </div>
      <div className='mt-2 relative'>
        <input
          required={required}
          value={value}
          onChange={onChange}
          onInput={onInput}
          className='bg-gray-100 dark:text-darked-200 w-full border border-solid border-gray-300 rounded-md py-1 px-2 text-[1.2rem]'
          id={id || `input-${idLbale}`}
          placeholder={label}
          type={inputPass ? 'password' : 'text'}
        />
        {password
          ? (
            <div
              onClick={() => {
                setInputPass((old) => !old);
              }}
              className='text-gray-500 absolute right-2 top-2 cursor-pointer'
            >
              {inputPass ? <EyeIcon /> : <EyeCloseIcon />}
            </div>
          )
          : null}
      </div>
    </div>
  );
}
