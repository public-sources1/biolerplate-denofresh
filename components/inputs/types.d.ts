import { JSX } from 'preact';
export type eventTypes = JSX.TargetedEvent<HTMLInputElement>;
export type onActionTypes = (event: eventTypes) => void;

export type inputTypes = {
  icon?: Element;
  password?: true;
  required?: true;
  label?: string;
  id?: string;
  onChange?: onActionTypes;
  onInput?: onActionTypes;
  value?: string;
};

export type inputFileTypes = {
  id?: string;
  onChange?: onActionTypes;
  onInput?: onActionTypes;
  multiple?: true;
  disabled?: true;
  placeholder?: string;
  accept?: string;
};
