import { inputFileTypes } from '@components/inputs/types.d.ts';

export default function (props: inputFileTypes) {
  const idDefault = props.id || 'thtyjty';
  return (
    <div className=''>
      <div className='text-center relative overflow-hidden bg-gray-100 rounded-lg py-2 px-5 hover:bg-gray-200 cursor-pointer'>
        <label
          className='text-gray-800 cursor-pointer'
          for={idDefault}
        >
          {props.placeholder}
        </label>
        <input
          id={idDefault}
          className='absolute opacity-0'
          onChange={props.onChange}
          onInput={props.onInput}
          accept={props.accept}
          type='file'
          multiple={props.multiple}
          disabled={props.disabled}
        />
      </div>
      <div className='mt-2'>
        <p className='text-[.7rem]'>
          SVG, PNG, JPG or GIF (MAX. 800x400px).
        </p>
      </div>
    </div>
  );
}
