import { iconTypes } from '@components/icons/types.d.ts';

export default ({ size, color }: iconTypes) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    width={size || '24'}
    height={size || '24'}
    viewBox='0 0 24 24'
    stroke-width='2.5'
    stroke={color || 'currentColor'}
    fill='none'
    stroke-linecap='round'
    stroke-linejoin='round'
  >
    <path stroke='none' d='M0 0h24v24H0z' fill='none' />
    <path d='M8 4m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z' />
    <path d='M16 16v2a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2v-8a2 2 0 0 1 2 -2h2' />
  </svg>
);
