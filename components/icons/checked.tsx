import { iconTypes } from '@components/icons/types.d.ts';

export default ({ size, color }: iconTypes) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    width={size || '24'}
    height={size || '24'}
    viewBox='0 0 24 24'
    strokeWidth='2'
    stroke={color || 'currentColor'}
    fill='none'
    strokeLinecap='round'
    strokeLinejoin='round'
  >
    <path stroke='none' d='M0 0h24v24H0z' fill='none' />
    <path d='M12 12m-9 0a9 9 0 1 0 18 0a9 9 0 1 0 -18 0' />
    <path d='M9 12l2 2l4 -4' />
  </svg>
);
