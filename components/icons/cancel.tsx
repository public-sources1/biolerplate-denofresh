import { iconTypes } from '@components/icons/types.d.ts';

export default ({ size, color }: iconTypes) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    class='icon icon-tabler icon-tabler-x'
    width={size || '24'}
    height={size || '24'}
    viewBox='0 0 24 24'
    stroke-width='2.5'
    stroke={color || 'currentColor'}
    fill='none'
    strokeLinejoin='round'
    strokeLinecap='round'
  >
    <path stroke='none' d='M0 0h24v24H0z' fill='none' />
    <path d='M18 6l-12 12' />
    <path d='M6 6l12 12' />
  </svg>
);
