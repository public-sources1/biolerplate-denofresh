export type iconTypes = {
  color?: string;
  size?: number | string;
};
