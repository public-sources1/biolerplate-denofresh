import { JSX } from 'preact';

export type buttonTypes = JSX.HTMLAttributes<HTMLButtonElement> & {
  text: string;
  color?: 'success' | 'info' | 'error' | 'warning' | 'primary';
};

export type linkSimpleTypes = {
  link: string;
  target?: 'blank';
  text: string;
};

export type iconButtonTypes = { children: JSX.Element; onClick?: () => void; className?: string };

export type externalLinkTypes = {
  href: string;
  text: string;
};
