import { buttonTypes } from '@components/buttons/types.d.ts';

export default function Button(props: buttonTypes) {
  let typeButton = '';

  if (!props.color || props.color === 'success' as buttonTypes['color']) {
    typeButton =
      `dark:bg-primary-dark bg-primary-main text-white hover:border-primary-dark dark:hover:border-primary-light border-transparent`;
  }
  if (props.color === 'primary' as buttonTypes['color']) {
    typeButton =
      `dark:bg-blue-900 bg-blue-400 text-white hover:border-blue-600 dark:hover:border-blue-400 border-transparent dark:border-blue-700 dark:hover:bg-blue-800`;
  }
  if (props.color === 'info' as buttonTypes['color']) {
    typeButton =
      `bg-transparent text-gray-600 dark:text-white hover:border-gray-800 dark:hover:bg-gray-700 hover:bg-gray-200 dark:hover:border-blue-400 border-gray-400 dark:border-blue-gray-200`;
  }
  return (
    <button
      role='button'
      className={`disabled:opacity-50 py-1 px-3 mr-1 mb-1 rounded-md inline-block transition-all duration-300 border-2 active:scale-105 ${typeButton}`}
      {...props}
    >
      {props.text}
    </button>
  );
}
