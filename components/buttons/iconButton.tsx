import { iconButtonTypes } from '@components/buttons/types.d.ts';

export default ({ children, onClick, className }: iconButtonTypes) => (
  <div
    role='button'
    onClick={onClick}
    className={`active:scale-115 cursor-pointer rounded-[50%] hover:bg-gray-200 dark:hover:bg-black dark:bg-opacity-20 transition-all duration-[.5s] border-2 border-solid border-transparent hover:border-gray-100 dark:hover:border-gray-900 ${className}`}
    tabIndex={1}
  >
    {children}
  </div>
);
