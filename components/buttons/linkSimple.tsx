import { linkSimpleTypes } from '@components/buttons/types.d.ts';
import createLinkWithLng from '@helpers/navigate/createLinkWithLng.ts';

export default ({ link, target, text }: linkSimpleTypes) => {
  const linkOverWrite = link.match(/javascript/) ? link : createLinkWithLng(link);
  return (
    <a
      className='underline text-gray-500 hover:text-blue-800 transition-all duration-500'
      href={linkOverWrite}
      target={target || ''}
      f-partial={linkOverWrite}
    >
      {text}
    </a>
  );
};
