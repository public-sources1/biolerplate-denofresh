import { LinkIcon } from '@components/icons/index.ts';
import { externalLinkTypes } from '@components/buttons/types.d.ts';

export default ({ href, text }: externalLinkTypes) => {
  return (
    <a
      target='_blank'
      href={href}
      className='inline-flex items-center text-blue-900 dark:text-blue-200 font-bold underline'
    >
      <LinkIcon size='17' />
      {text}
    </a>
  );
};
