import { titleTypes } from '@components/text/titles/types.d.ts';

export default ({ text, align }: titleTypes) => (
  <h2
    style={{
      textAlign: align || 'center',
    }}
    className='text-[2rem] mb-5'
  >
    {text}
  </h2>
);
