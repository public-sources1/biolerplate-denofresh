import { titleTypes } from '@components/text/titles/types.d.ts';

export function MainTitle({ text }: titleTypes) {
  return <h1 className='text-[2.7rem] text-black text-center sm:text-left'>{text}</h1>;
}
