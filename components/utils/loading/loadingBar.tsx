import { loadingDeafultTypes } from '@components/utils/loading/types.d.ts';

export default function (props: loadingDeafultTypes) {
  return (
    <div className='bg-white rounded-md overflow-hidden'>
      <link rel='stylesheet' href='/css/loadingBar.css' />
      <div
        style={{
          width: `${props.width || 100}%`,
        }}
        className="ss-loading-bar relative top-0 shim-blue bg-blue-500 text-center rounded-md after:absolute after:content-['']"
      >
        {props.text || ''}
      </div>
    </div>
  );
}
