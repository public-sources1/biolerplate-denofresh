export type loadingDeafultTypes = {
  text?: string;
  width?: number | string;
};

export type loadingTypes = {
  color?: string;
  size?: number;
  text?: string;
};
