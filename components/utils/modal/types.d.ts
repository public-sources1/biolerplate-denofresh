import { JSX } from 'preact/jsx-runtime';

export type modalTypes = {
  onClose?: () => void;
  title: string;
  children?: JSX.Element | string;
  footer?: JSX.Element;
  open?: boolean;
};
