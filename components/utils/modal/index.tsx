import SubTitle from '@components/text/titles/subTitle.tsx';
import { CancelIcon } from '@components/icons/index.ts';
import IconButton from '@components/buttons/iconButton.tsx';
import { modalTypes } from '@components/utils/modal/types.d.ts';

export default function ({ onClose, title, children, footer, open }: modalTypes) {
  return (open
    ? (
      <>
        <div onClick={onClose} className='fixed inset-0 z-50 bg-black bg-opacity-40' />
        <div
          role='dialog'
          aria-modal='true'
          className='absolute inset-x-0 top-[10dvh] m-auto z-50 max-w-[800px] w-[98%]'
        >
          <div className='bg-white dark:bg-darked-100 dark:text-gray-100 p-5 rounded-lg'>
            <div tabIndex={1} className='top-1 right-1 absolute'>
              <IconButton onClick={onClose}>
                <CancelIcon />
              </IconButton>
            </div>
            <header>
              <SubTitle align='center' text={title} />
            </header>
            <main>
              {children}
            </main>
            {footer ? <footer>{footer}</footer> : null}
          </div>
        </div>
      </>
    )
    : null);
}
