export type alertTypes = {
  text: string;
  severity?: 'success' | 'error' | 'warning';
};
