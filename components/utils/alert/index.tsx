import { AlertIcon, CheckedIcon } from '@components/icons/index.ts';
import { alertTypes } from '@components/utils/alert/types.d.ts';

export default ({ text, severity }: alertTypes) => {
  const colorLight = severity === 'success' ? 'green-300' : 'red-300';
  const colorHard = severity === 'success' ? 'green-800' : 'red-800';
  return (
    <div
      role='alert'
      className={`box-border flex gap-2 bg-${colorLight} bg-opacity-10 py-2 px-3 rounded-md border border-${colorHard}`}
    >
      <div className={`text-${colorHard}`}>
        {(severity ?? 'success') === 'success' ? <CheckedIcon /> : <AlertIcon />}
      </div>
      <div>{text}</div>
    </div>
  );
};
