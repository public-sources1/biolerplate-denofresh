import i18next, { changeLanguage, t } from 'i18next';
import '@helpers/i18n/config.ts';

export default function () {
  const selectSpain = i18next.language === 'es';
  function changeLang(lng: string) {
    document.location = `${document.location.pathname}?lng=${lng === 'es' ? 'en' : 'es'}`;
  }

  return (
    <div
      className='flex items-center gap-2 hover:bg-gray-800 transition duration-[.8s] p-[2px] rounded-md'
      onClick={() => changeLang(i18next.language)}
    >
      <img src={`/img/flag_${selectSpain ? 'uk' : 'spain'}.png`} alt={t('change_language')} />
      <div className='text-gray-700 bg-gray-100 rounded-md px-1'>
        {selectSpain ? 'en' : 'es'}
      </div>
    </div>
  );
}
