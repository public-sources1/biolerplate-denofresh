import { APP_NAME } from '@constants';

import { t } from 'i18next';

export default function () {
  return (
    <footer>
      <div>
        {t('footer:all_right_reserved', {
          currentDate: new Date().getFullYear(),
          appName: APP_NAME,
        })}
      </div>
      <img src='/img/logo.png' alt={APP_NAME} />
    </footer>
  );
}
