import { MainTitle } from '@components/text/titles/index.tsx';
import type { loginOutCardsTypes } from '@components/layout/cards/types.ts';

export default function ({ children, title }: loginOutCardsTypes) {
  return (
    <div className='mt-5 mx-auto w-[96%] max-w-[500px] flex flex-col gap-y-5 bg-white py-10 px-10 rounded-lg border shadow-md shadow-black'>
      <MainTitle text={title} />
      {children}
    </div>
  );
}
