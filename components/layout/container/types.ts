import { JSX } from 'preact';

export type contentMapTypes = {
  children: JSX.Element;
};

export type homeContainerTypes = {
  children: JSX.Element;
};
