import { homeContainerTypes } from '@components/layout/container/types.ts';

export default ({ children }: homeContainerTypes) => (
  <div class='max-w-screen-md mx-auto flex flex-col items-center justify-center'>{children}</div>
);
