import { ListTypes } from '@components/layout/list/types.d.ts';

export default function List(props: ListTypes) {
  return (
    <div
      style={{
        maxHeight: `${props.height || 80}dvh`,
      }}
      className='overflow-y-auto'
    >
      <ul>
        {props.items.length
          ? props.items.map((item, key: number) => (
            <li
              style={{
                cursor: props.pointer ? 'pointer' : 'inherit',
              }}
              className='hover:bg-gray-200 dark:hover:bg-darked-500 transition-all duration-[.7s] border-b-1 border-x-1 border-solid border-gray-200 dark:border-darked-200 py-2 px-2'
              key={key}
              onClick={() => {
                if (props.onClick) {
                  props.onClick(item.id);
                }
              }}
              tabIndex={key}
            >
              {item.label}
            </li>
          ))
          : (
            <li
              style={{
                cursor: props.pointer ? 'pointer' : 'inherit',
              }}
              className='hover:bg-gray-200 transition-all duration-[.7s] border-b-1 border-x-1 border-solid border-gray-200 py-2 px-2'
              tabIndex={1}
            >
              {props.noLength}
            </li>
          )}
      </ul>
    </div>
  );
}
