export type itemTypes = {
  label: string;
  onClick?: () => void;
  id: string;
};

export type ListTypes = {
  items: itemTypes[];
  height?: number;
  onClick?: (id: string) => void;
  pointer?: true;
  noLength?: string;
};
