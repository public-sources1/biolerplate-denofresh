import { Options } from '$fresh/plugins/twind.ts';
//import more colors from twind
// import * as colors from 'twind/colors';

const customColors = {
  darked: {
    100: '#2b2b2b',
    200: '#202020',
    300: '#1c3439',
    400: '#061717',
    500: '#22262f',
    900: '#000136',
  },
  primary: {
    main: '#26be96',
    dark: '#178e6f',
    light: '#2fe8b7',
  },
  // secondary: {
  //   main: '',
  //   dark: '',
  //   light: '',
  // },
} as const;

export const fontSizes = {
  min: '.8rem',
  normal: '1rem',
  big: '1.5rem',
  longer: '2.rem',
};

export default {
  selfURL: import.meta.url,
  darkMode: 'class',
  mode: 'silent',
  theme: {
    extend: {
      fontFamily: {
        sans: 'Roboto, sans-serif',
        'proxima-nova': '"Proxima Nova"',
      },
      colors: {
        // ...colors,
        ...customColors,
      },
    },
  },
} as Options;
