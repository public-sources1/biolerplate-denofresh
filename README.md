# Deno Fresh Boilerplate

![Banner project svg](./docs/banner.svg)

## Description

This is a project in [denojs](https://deno.com/), [fresh](https://fresh.deno.dev/), [i18next for translate to multiple language](https://www.i18next.com/), [Preact js](https://preactjs.com), and [Twind css](https://twind.dev/) that is a package very similar to [tailwind css](https://tailwindcss.com/) but more light for your projects. The finally for this repo is that you can use this boilerplate for start very quick you projects.

We use **[import_map](./import_map.json)** for create a alias in import inside of code, but also y for have in same side all package, you can edit that and reorder like you desire.

## scripts

- For format code: `deno task format`
- For check code: `deno task check`
- For start server: `deno task start`
- For build project: `deno task build`
- For preview project: `deno task preview`
- For update denojs: `deno task update`

## Format code

You can edit the formatter code change properties like `lineWidth` among others.

## Twind extra settings

I increase quantity of colors by default from Twind import package `https://esm.sh/twind@0.16.19/colors` and you can do more things in file [twind.config.ts](./twind.config.ts), [here are all information about of that](https://twind.dev/handbook/configuration.html#theme)

Colors in theme by deafult in twindJs:

```
black,
white,
gray,
red,
yellow,
green,
blue,
indigo,
purple,
```

## Components

I create component like buttons, inputs, alerts among other for you, but if you want delete that.

## Signals

I create a example for use hooks `signal` and `useSignal` for you check the difference and create a folder called store for save this global state
